include chocolatey
# Set up regular Puppet runs
#file { '/usr/local/bin/run-puppet':
#  source => '/etc/puppetlabs/code/environments/production/files/run-puppet.sh',
#  mode   => '0755',
#}

#cron { 'run-puppet':
#  command => '/usr/local/bin/run-puppet',
#  hour    => '*',
#  minute  => '*/15',
#}

file { 'c:/Windows/temp/hello.txt':
  ensure  => file,
  content => "hello alex\n",
}

package { '7zip':  
#  #ensure   => '1.23.3',
  provider => chocolatey,
}

group { 'Avengers':  
  ensure => present,
}

user { 'TonyStark2':  
  ensure   => present,
  password => 'password123',
  groups   => ['Avengers'],
}
